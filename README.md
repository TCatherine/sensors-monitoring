# Sensors monitoring
## Среда
Были развернуты три вертулки: 3 Ubuntu 22.04 с помощью VMware Workstation. Сетевое взаимодействие настроено средствами VMware Workstation. Тип сетевого взаимодействия - NAT. С помощью DHCP были выданы IP адреса:
1. 192.168.12.136 (VM1)
2. 192.168.12.137 (VM2)
3. 192.168.12.135 (VM3)

Пакеты:
- python3, pip3
- docker, docker-compose (python packages)

Также на виртуальной машине нужно разрешить:

## Установка (Pre-install)
Будем считать, что Sonatype Nexus уже развернут на сервере VM1.

Ниже приведена команда, чтобы развернуть Sonatype Nexus.
```
docker run -d -p 8081:8081 -p 8082:8082 --name nexus sonatype/nexus3:3.14.0
```

```
ssh-copy-id -i ~/.ssh/id_rsa.pub root@192.168.12.138
```

## Сборка образов
Собрать образ сенсора можно через ansible:
```
cd playbooks && ansible-playbool build.yml
```
Разворачивание образов:
```
cd playbooks && ansible-playbool deploy.yml
```