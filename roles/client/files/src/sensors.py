import random
import time
import json
import os
import logging

from paho.mqtt import client as mqtt_client
from generator import Generator


logging.basicConfig(level=logging.INFO, format="%(levelname)s %(message)s")


class Sensor():
    def __init__(self):
        self.broker_address = os.getenv("BROKER")
        self.broker_port = int(os.getenv("BROKER_PORT"))
        self.name = os.getenv("SENSOR_NAME")
        self.client = self.connect_mqtt()
        self.client.loop_start()
        logging.info(f"Start {self.name}")

    def connect_mqtt(self):
        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                logging.info("Connected to MQTT Broker!")
            else:
                logging.error("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.name)
        client.on_connect = on_connect
        client.connect(self.broker_address, self.broker_port)
        return client

    def publish(self, name: str, value: int, topic: str):
        msg = json.dumps({"name": name, "value": value})
        result = self.client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            logging.info(f"Send `{msg}` to topic `{topic}`")
        else:
            logging,info(f"Failed to send message to topic {topic}")
    
    def get_next_value(a: int) -> int:
        if self.formula_type:
            return random.randint() % self.year


class TemperatureSensor(Sensor):
    def __init__(self):
        self.topic = "sensor/temperature"
        self.name = os.getenv("SENSOR_NAME")
        self.quant = int(os.getenv("SENSOR_QUANT"))
        self.generator = Generator(os.getenv("SENSOR_FORMULA_TYPE"))
        super().__init__()

    def publish(self):
        while 1:
            value = self.generator.get_next_value()
            super().publish(self.name, value, self.topic)
            time.sleep(self.quant)


class PressureSensor(Sensor):
    def __init__(self):
        self.topic = "sensor/pressure"
        self.name = os.getenv("SENSOR_NAME")
        self.quant = int(os.getenv("SENSOR_QUANT"))
        self.generator = Generator(os.getenv("SENSOR_FORMULA_TYPE"))
        super().__init__()

    def publish(self):
        while 1:
            value = self.generator.get_next_value()
            super().publish(self.name, value, self.topic)
            time.sleep(self.quant)


class CurrentСonsumptionSensor(Sensor):
    def __init__(self):
        self.topic = "sensor/current_consumption"
        self.name = os.getenv("SENSOR_NAME")
        self.quant = int(os.getenv("SENSOR_QUANT"))
        self.generator = Generator(os.getenv("SENSOR_FORMULA_TYPE"))
        super().__init__()

    def publish(self):
        while 1:
            value = self.generator.get_next_value()
            super().publish(self.name, value, self.topic)
            time.sleep(self.quant)


class HumiditySensor(Sensor):
    def __init__(self):
        self.topic = "sensor/humidity"
        self.name = os.getenv("SENSOR_NAME")
        self.quant = int(os.getenv("SENSOR_QUANT"))
        self.generator = Generator(os.getenv("SENSOR_FORMULA_TYPE"))
        super().__init__()

    def publish(self):
        while 1:
            value = self.generator.get_next_value()
            super().publish(self.name, value, self.topic)
            time.sleep(self.quant)


if __name__ == "__main__":
    sensor = None
    sensor_type = os.getenv("SENSOR_TYPE")
    if sensor_type == "Temperature":
        sensor = TemperatureSensor()
    elif sensor_type == "Pressure":
        sensor = PressureSensor()
    elif sensor_type == "CurrentСonsumption":
        sensor = CurrentСonsumptionSensor()
    elif sensor_type == "Humidity":
        sensor = HumiditySensor()
    else:
        logging.error(f"Sensor type \"{sensor_type}\" not found")
        exit(1)
