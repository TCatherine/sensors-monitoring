import random
import math

class Generator():
    def __init__(self, formula_type: str):
        self.formula_type = formula_type
        # Parameters for generation next number
        self.m = 681787
        self.c = 7
        self.a = 8
        self.d = 2000
        self.x = random.randrange(2000)
        if self.formula_type == "SCG":
            self.prev_x = random.randrange(2000)

    def get_next_value(self):
        if self.formula_type == "LCG":
            x = (self.a* self.x + self.c) % self.m 
        elif self.formula_type == "QCG":
            x = (self.d*self.x**2 + self.a*self.x + self.c) % self.m
        elif self.formula_type == "SCG":
            self.c = math.trunc((self.a*self.prev_x + self.c)/self.m)
            self.prev_x = self.x
            x = (self.a*self.x + self.c) % self.m
        return x
